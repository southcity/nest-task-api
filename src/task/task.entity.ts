import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

export enum TaskState {
  Incomplete,
  Pending,
  Completed,
}

@Entity()
export class Task {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 64, nullable: false })
  title: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  description?: string;

  @Column({ type: 'enum', enum: TaskState, default: TaskState.Incomplete })
  state: TaskState;
}
