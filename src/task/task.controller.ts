import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { CreateTaskDTO, UpdateTaskDTO } from './task.dto';
import { Task } from './task.entity';
import { TaskService } from './task.service';

@Controller('tasks')
export class TaskController {
  public constructor(private readonly taskService: TaskService) {}

  @Post()
  async create(@Body() dto: CreateTaskDTO): Promise<Task> {
    return this.taskService.create(dto);
  }

  @Get()
  async findAll(): Promise<Task[]> {
    return this.taskService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<Task> {
    return this.taskService.findOne(id);
  }

  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() dto: UpdateTaskDTO,
  ): Promise<Task> {
    return this.taskService.update(id, dto);
  }

  @Delete(':id')
  async delete(@Param('id') id: number): Promise<Task> {
    return this.taskService.delete(id);
  }
}
