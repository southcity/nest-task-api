import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTaskDTO, UpdateTaskDTO } from './task.dto';
import { Task } from './task.entity';

@Injectable()
export class TaskService {
  public constructor(
    @InjectRepository(Task)
    private readonly taskRepository: Repository<Task>,
  ) {}

  async create(dto: CreateTaskDTO): Promise<Task> {
    const task = new Task();
    task.title = dto.title;
    task.description = dto.description;

    return this.taskRepository.save(task);
  }

  async findAll(): Promise<Task[]> {
    return this.taskRepository.find();
  }

  async findOne(id: number): Promise<Task> {
    return this.taskRepository.findOne(id);
  }

  async update(id: number, dto: UpdateTaskDTO): Promise<Task> {
    const task: Task = await this.findOne(id);
    task.title = dto.title ?? task.title;
    task.description = dto.description ?? task.description;
    task.state = dto.state ?? task.state;

    return this.taskRepository.save(task);
  }

  async delete(id: number): Promise<Task> {
    const task: Task = await this.findOne(id);

    return this.taskRepository.remove(task);
  }
}
